```
$ sudo docker-compose up -d
$ curl localhost:8888/js_hello   #=> Hello njs!
$ curl localhost:8888/lua_hello  #=> <p>Hello Lua!</p>
$ sudo docker-compose down
```
