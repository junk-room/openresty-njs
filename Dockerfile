# OpenrestyのDockerイメージにnjsモジュールを導入する
# https://qiita.com/uokada/items/67bc143292a8432808b8

FROM openresty/openresty:1.15.8.2-alpine-fat AS builder

# Our NCHAN version
ENV NGINX_VERSION 1.15.8
ENV NJS_VERSION 0.3.7

# For latest build deps, see https://github.com/nginxinc/docker-nginx/blob/master/mainline/alpine/Dockerfile
RUN apk add --no-cache --virtual .build-deps \
  gcc \
  libc-dev \
  make \
  openssl-dev \
  pcre-dev \
  zlib-dev \
  linux-headers \
  curl \
  gnupg \
  libxslt-dev \
  gd-dev \
  geoip-dev \
  readline readline-dev libedit-dev

# Download sources
RUN curl -L "http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz" -o nginx.tar.gz \
        && curl -L "https://github.com/nginx/njs/archive/${NJS_VERSION}.tar.gz" -o njs.tar.gz

# Reuse same cli arguments as the nginx:alpine image used to build
RUN CONFARGS=$(nginx -V 2>&1 | sed -n -e 's/^.*arguments: //p') \
    tar -zxC /usr/local -f nginx.tar.gz && \
    tar -xzvf "njs.tar.gz" -C /usr/local/nginx-${NGINX_VERSION} && \
    NJSDIR="/usr/local/nginx-${NGINX_VERSION}/njs-${NJS_VERSION}/nginx" && \
    cd /usr/local/nginx-${NGINX_VERSION} && \
   ./configure --with-compat $CONFARGS --add-dynamic-module=$NJSDIR && \
    make modules && make install && \
    cd "/usr/local/nginx-${NGINX_VERSION}/njs-${NJS_VERSION}" && \
    ./configure && make njs && \
    cp build/njs /usr/local/bin/njs

FROM openresty/openresty:1.15.8.2-6-alpine
# Extract the dynamic module NCHAN from the builder image
COPY --from=builder /usr/local/nginx/modules/ngx_http_js_module.so /usr/local/openresty/nginx/modules/ngx_http_js_module.so
COPY --from=builder /usr/local/bin/njs /usr/local/bin/njs
RUN  apk add --no-cache --virtual .build-deps pcre-dev

COPY nginx.conf   /usr/local/openresty/nginx/conf/nginx.conf
COPY default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
STOPSIGNAL SIGTERM
CMD ["nginx", "-g", "daemon off;"]
